package patterns.station;

import java.util.Observable;

/**
 * Created by ipolyakov on 09.11.2017.
 */
public class WeatherData extends Observable {
    private float temperature;
    private float pressure;
    private float humidity;

    public void setMeasurements(float temp, float pressure, float humidity){
        this.temperature = temp;
        this.pressure = pressure;
        this.humidity = humidity;
        setChanged();
        notifyObservers();
    }

    public float getTemperature() {
        return temperature;
    }

    public float getPressure() {
        return pressure;
    }

    public float getHumidity() {
        return humidity;
    }
}
