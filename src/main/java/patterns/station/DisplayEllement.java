package patterns.station;

/**
 * Created by ipolyakov on 09.11.2017.
 */
public interface DisplayEllement {
    void display();
}
