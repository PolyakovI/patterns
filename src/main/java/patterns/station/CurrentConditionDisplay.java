package patterns.station;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by ipolyakov on 09.11.2017.
 */
public class CurrentConditionDisplay implements DisplayEllement,Observer {
    private final Logger logger = LogManager.getLogger(CurrentConditionDisplay.class);
    private float temp;
    private float pressure;
    private float humidity;

    public CurrentConditionDisplay(Observable observable) {
        observable.addObserver(this);
    }

    @Override
    public void display() {

        logger.printf(Level.INFO,"Current condition: temp=%.1f, pressure=%.0f, humidity=%.3f",temp,pressure,humidity);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData){
            WeatherData weatherData = (WeatherData) o;
            this.temp = weatherData.getTemperature();
            this.pressure = weatherData.getPressure();
            this.humidity = weatherData.getHumidity();
            display();
        }
    }
}
