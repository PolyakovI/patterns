package patterns.station;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by ipolyakov on 09.11.2017.
 */
public class ForecastDisplay implements Observer,DisplayEllement {
    private final Logger logger = LogManager.getLogger(ForecastDisplay.class);
    private float currentPressure = 720f;
    private float lastPressure;

    public ForecastDisplay(Observable observable) {
        observable.addObserver(this);
    }

    @Override
    public void display() {
        logger.info("lastPressure=" + lastPressure +
                "currentPressure=" + currentPressure);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData){
            WeatherData weatherData = (WeatherData) o;
            lastPressure = currentPressure;
            currentPressure = weatherData.getPressure();
        }
        display();
    }
}
