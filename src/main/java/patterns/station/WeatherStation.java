package patterns.station;

/**
 * Created by ipolyakov on 09.11.2017.
 */
public class WeatherStation {


    public static void main(String[] args) {


        WeatherData weatherData = new WeatherData();
        new CurrentConditionDisplay(weatherData);
        new HeatIndexDisplay(weatherData);
        new ForecastDisplay(weatherData);

        weatherData.setMeasurements(21,740,31.45f);
        weatherData.setMeasurements(23,750,27.5f);
        weatherData.setMeasurements(24,730,22.45f);
    }
}
