package patterns.starbuzz;

public abstract class Beverage {

    private Price price;
    private size portion;
    private String description = "Unknown beverage";
    private size defaultSize = size.MEDIUM;

    public enum size{
        SMALL,
        MEDIUM,
        LARGE
    }


    public double cost(){
        switch (portion){
            case SMALL:
                return price.getSmallPrice();
            case MEDIUM:
                return price.getMediumPrice();
            case LARGE:
                return price.getLargePrice();
        }
        throw new IllegalArgumentException("No cost for portion`s size " + portion + " for " + this.getDescription());
    }


    public Beverage(){
    }

    public Beverage(size portion) {
        this.portion = portion;
    }

    public size getDefaultSize() {
        return defaultSize;
    }

    public void setDefaultSize(size defaultSize) {
        this.defaultSize = defaultSize;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription(){
        return description;
    }

    public size getPortion() {
        return portion;
    }

    public void setPortion(size portion) {
        this.portion = portion;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return this.getDescription() + " - " + String.format("%.2f",this.cost()) + "$";
    }
}
