package patterns.starbuzz.condiments;

import patterns.starbuzz.Beverage;
import patterns.starbuzz.CondimentDecorator;
import patterns.starbuzz.Price;

public class Whip extends CondimentDecorator {

    private static Price whipPrice = new Price(0.5, 0.10, 0.15);

    public Whip(Beverage beverage) {
        super(beverage);
        super.setCondimentDescription("Whip");
        super.setCondimentPrice(whipPrice);
    }
}
