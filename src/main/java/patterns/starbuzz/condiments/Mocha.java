package patterns.starbuzz.condiments;

import patterns.starbuzz.Beverage;
import patterns.starbuzz.CondimentDecorator;
import patterns.starbuzz.Price;

public class Mocha extends CondimentDecorator {

    private static Price mochaPrice = new Price(0.10, 0.15, 0.20);

    public Mocha(Beverage beverage) {
        super(beverage);
        super.setCondimentDescription("Mocha");
        super.setCondimentPrice(mochaPrice);
    }

}
