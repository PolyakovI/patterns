package patterns.starbuzz.condiments;

import patterns.starbuzz.Beverage;
import patterns.starbuzz.CondimentDecorator;
import patterns.starbuzz.Price;

public class Soy extends CondimentDecorator{

    private static Price soyPrice = new Price(0.15, 0.20, 0.25);


    public Soy(Beverage beverage) {
        super(beverage);
        super.setCondimentDescription("Soy");
        super.setCondimentPrice(soyPrice);
    }

}
