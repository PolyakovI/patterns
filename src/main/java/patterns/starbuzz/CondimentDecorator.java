package patterns.starbuzz;

public abstract class CondimentDecorator extends Beverage {

    private Beverage beverage;
    private String condimentDescription = "Unknown condiment";
    private Price condimentPrice;

    public CondimentDecorator(Beverage beverage) {
        this.beverage = beverage;
    }


    @Override
    public double cost() {
            switch (beverage.getPortion()){
                case SMALL:
                    return getCondimentPrice().getSmallPrice() + getBeverage().cost();
                case MEDIUM:
                    return getCondimentPrice().getMediumPrice() + getBeverage().cost();
                case LARGE:
                    return getCondimentPrice().getLargePrice() + getBeverage().cost();
            }
            throw new IllegalArgumentException("No cost for portion`s size " + getBeverage().getPortion() + " for " + this.getDescription());
    }


    public Price getCondimentPrice() {
        if (condimentPrice != null) {
            return condimentPrice;
        }
        throw new IllegalArgumentException(" You have no price for condiment" + getCondimentDescription() + "for" + getBeverage().toString());
    }

    protected void setCondimentPrice(Price condimentPrice) {
        this.condimentPrice = condimentPrice;
    }

    protected void setCondimentDescription(String condimentDescription) {
        this.condimentDescription = condimentDescription;
    }

    public Beverage getBeverage() {
        return beverage;
    }

    public void setBeverage(Beverage beverage) {
        this.beverage = beverage;
    }

    public String getCondimentDescription() {
        return condimentDescription;
    }

    @Override
    public String getDescription(){
        return getBeverage().getDescription() + ", " + getCondimentDescription();
    }

    @Override
    public size getPortion() {
        return getBeverage().getPortion();
    }

    @Override
    public void setPortion(size portion) {
        getBeverage().setPortion(portion);
    }
}
