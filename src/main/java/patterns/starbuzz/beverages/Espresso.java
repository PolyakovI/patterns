package patterns.starbuzz.beverages;

import patterns.starbuzz.Beverage;
import patterns.starbuzz.Price;

public class Espresso extends Beverage {
    private static Price espressoPrice = new Price(1.49,1.99,2.49);

    public Espresso() {
        super();
        super.setPortion(super.getDefaultSize());
        super.setDescription("It is Espresso");
        super.setPrice(espressoPrice);
    }

    public Espresso(size portion) {
        super(portion);
        super.setDescription("It is Espresso");
        super.setPrice(espressoPrice);

    }
}
