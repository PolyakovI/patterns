package patterns.starbuzz.beverages;

import patterns.starbuzz.Beverage;
import patterns.starbuzz.Price;

public class Cappuccino extends Beverage {
    private static Price cappuccinoPrice = new Price(0.9,1.1,1.3);

    public Cappuccino() {
        super();
        super.setPortion(super.getDefaultSize());
        super.setDescription("It is Cappuccino");
        super.setPrice(cappuccinoPrice);

    }

    public Cappuccino(size portion) {
        super(portion);
        super.setDescription("It is Cappuccino");
        super.setPrice(cappuccinoPrice);
    }

}
