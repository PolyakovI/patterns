package patterns.starbuzz;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import patterns.starbuzz.beverages.Cappuccino;
import patterns.starbuzz.beverages.Espresso;
import patterns.starbuzz.condiments.Mocha;
import patterns.starbuzz.condiments.Soy;
import patterns.starbuzz.condiments.Whip;

public class MiniStarbuzzCafe {
    private static final Logger logger = LogManager.getLogger(MiniStarbuzzCafe.class);

    public static void main(String[] args) {
        Beverage beverage1 = new Espresso();
        logger.info(beverage1);


        Beverage beverage2 = new Cappuccino();
        beverage2 = new Mocha(beverage2);
        beverage2 = new Mocha(beverage2);
        beverage2 = new Whip(beverage2);
        logger.info(beverage2);

        Beverage beverage3 = new Cappuccino(Beverage.size.LARGE);
        beverage3 = new Soy(beverage3);
        logger.info(beverage3);
    }
}
