package patterns.smarthouse;

import patterns.smarthouse.commands.Command;
import patterns.smarthouse.commands.NoCommand;
import patterns.smarthouse.devices.DeviceForRemoteControl;
import patterns.smarthouse.devices.NoDevice;

public class RemoteControl {
    private DeviceForRemoteControl[] deviceForRemoteControls;
    private Command lastCommand;
    RemoteControl(int number_devices) {
        deviceForRemoteControls = new DeviceForRemoteControl[number_devices];
        for (int i = 0; i < deviceForRemoteControls.length; i++) {
            deviceForRemoteControls[i] = new NoDevice();
        }
        lastCommand = new NoCommand();
    }
    void onButtonWasPushed(int deviceIndex){
        Command onCommand = deviceForRemoteControls[deviceIndex].getOnCommand();
        onCommand.execute();
        lastCommand = onCommand;

    }
    void offButtonWasPushed(int deviceIndex){
        Command offCommand = deviceForRemoteControls[deviceIndex].getOffCommand();
        offCommand.execute();
        lastCommand = offCommand;
    }

    void undoButtonWasPushed(){
        lastCommand.undo();
    }

    void setDevice(int deviceIndex, DeviceForRemoteControl device){
        deviceForRemoteControls[deviceIndex] = device;}

    DeviceForRemoteControl[] getDeviceForRemoteControls() {
        return deviceForRemoteControls;
    }

}
