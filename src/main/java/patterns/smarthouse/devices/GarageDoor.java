package patterns.smarthouse.devices;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GarageDoor extends Device {
    private static final Logger logger = LogManager.getLogger(GarageDoor.class);

    public void up(){
        logger.info(String.format("Garage Door %s is Open", this.toString()));
    }
    public void down(){
        logger.info(String.format("Garage Door %s is Close ", this.toString()));
    }


}
