package patterns.smarthouse.devices;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Stereo extends Device {
    private int volume;
    private static final int DEFAULT_VOLUME = 10;
    private static final Type DEFAULT_TYPE = Type.RADIO;
    private Type type;

    public enum Type {
        CD,
        DVD,
        RADIO
    }

    public Stereo() {
        init();
    }

    public Stereo(String name) {
        super(name);
        init();
    }

    private void init() {
        volume = DEFAULT_VOLUME;
        type = DEFAULT_TYPE;
    }

    @Override
    String getName() {
        return String.format("%s    volume: %d, type: %s",super.getName(),volume,type.toString());
    }

    private static final Logger logger = LogManager.getLogger(Light.class);
    public void on(){
        logger.printf(Level.INFO,"Stereo %s -> on", this.getName());
    }
    public void off(){
        logger.printf(Level.INFO,"Stereo %s -> off", this.getName());
    }

    public void setCD(){ type = Type.CD;}
    public void setDVD(){type = Type.DVD;}
    public void setRadio(){type = Type.RADIO;}

    public void setVolume(int volume) {
        this.volume = volume;
    }

}
