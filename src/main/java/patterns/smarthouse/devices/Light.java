package patterns.smarthouse.devices;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Light extends Device {
    private static final Logger logger = LogManager.getLogger(Light.class);

    public Light() {
    }

    public Light(String name) {
        super(name);
    }

    public void on(){
        logger.printf(Level.INFO,"Light %s -> on", this.getName());
    }
    public void off(){
        logger.printf(Level.INFO,"Light %s -> off", this.getName());
    }
}
