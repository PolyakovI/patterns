package patterns.smarthouse.devices;

import patterns.smarthouse.commands.StereoOffCommand;
import patterns.smarthouse.commands.StereoOnWithCDCommand;

public class StereoInBedroom extends DeviceForRemoteControlImplementation{

    public StereoInBedroom(Stereo stereo) {
        super(stereo.getName(),new StereoOnWithCDCommand(stereo), new StereoOffCommand(stereo));
    }
}
