package patterns.smarthouse.devices;

import patterns.smarthouse.commands.Command;

public interface DeviceForRemoteControl {
    String getName();
    Command getOnCommand();
    Command getOffCommand();

}
