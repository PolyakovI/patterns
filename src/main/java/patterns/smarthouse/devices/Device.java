package patterns.smarthouse.devices;

public abstract class Device {
    private String name;

    String getName(){
        return name;
    }

    Device() {
        name = this.getClass().getSimpleName() + "@" + this.hashCode();
    }

    public Device(String name) {
        this.name = name;
    }

}
