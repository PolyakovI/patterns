package patterns.smarthouse.devices;

import patterns.smarthouse.commands.Command;
import patterns.smarthouse.commands.NoCommand;

public class NoDevice implements DeviceForRemoteControl{
    @Override
    public String getName() {
        return null;
    }

    @Override
    public Command getOnCommand() {
        return new NoCommand();
    }

    @Override
    public Command getOffCommand() {
        return new NoCommand();
    }
}
