package patterns.smarthouse.devices;

import patterns.smarthouse.commands.Command;

public class DeviceForRemoteControlImplementation implements DeviceForRemoteControl {
    private String name;
    private Command onCommand;
    private Command offCommand;

    DeviceForRemoteControlImplementation(String name,Command onCommand, Command offCommand) {
        this.name = name;
        this.onCommand = onCommand;
        this.offCommand = offCommand;
    }

    @Override
    public String getName() {
        return this.getClass().getCanonicalName();
    }

    @Override
    public Command getOnCommand() {
        return onCommand;
    }

    @Override
    public Command getOffCommand() {
        return offCommand;
    }
}
