package patterns.smarthouse.devices;

import patterns.smarthouse.commands.LightOffCommand;
import patterns.smarthouse.commands.LightOnCommand;

public class LightInKitchen extends DeviceForRemoteControlImplementation {

    public LightInKitchen(Light light) {
        super(light.getName(),new LightOnCommand(light),new LightOffCommand(light));
    }

}
