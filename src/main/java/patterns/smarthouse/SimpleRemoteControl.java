package patterns.smarthouse;

import patterns.smarthouse.commands.Command;

class SimpleRemoteControl {
    private Command slot1;
    private Command slot2;


    void setSlot1(Command command) {
        this.slot1 = command;
    }

    void setSlot2(Command slot2) {
        this.slot2 = slot2;
    }

    void buttonSlot1WasPressed() {
        slot1.execute();
    }
    void buttonSlot2WasPressed() {
        slot2.execute();
    }
}
