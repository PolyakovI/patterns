package patterns.smarthouse.commands;
public interface Command {
    void execute();
    void undo();
}
