package patterns.smarthouse.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import patterns.smarthouse.RemoteControl;

public class NoCommand implements Command {
    private static final Logger logger = LogManager.getLogger(RemoteControl.class);
    private static final String NO_DEVICE_MESSAGE = "No device for this slot";

    @Override
    public void execute() {
        logger.info(NO_DEVICE_MESSAGE);
    }

    @Override
    public void undo() {
        logger.info(NO_DEVICE_MESSAGE);
    }
}
