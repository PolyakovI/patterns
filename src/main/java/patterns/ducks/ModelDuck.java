package patterns.ducks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by ipolyakov on 08.11.2017.
 */
public class ModelDuck extends Duck {
    private static final Logger logger = LogManager.getLogger(ModelDuck.class);
    @Override
    public void display() {
        logger.info("I`m just a model(((");
    }

    public ModelDuck() {
        flyBehavior = new FlyNoWay();
        quackBehavior = new MuteQuack();
    }
}
