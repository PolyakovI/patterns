package patterns.ducks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by ipolyakov on 08.11.2017.
 */
public class FlyWithWings implements FlyBehavior {
    private static final Logger logger = LogManager.getLogger(FlyWithWings.class);
    @Override
    public void fly() {
        logger.info("I fling with wings");
    }
}
