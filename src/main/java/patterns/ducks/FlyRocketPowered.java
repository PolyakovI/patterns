package patterns.ducks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by ipolyakov on 08.11.2017.
 */
public class FlyRocketPowered implements FlyBehavior {
    private static final Logger logger = LogManager.getLogger(FlyRocketPowered.class);

    @Override
    public void fly() {
        logger.info("I`m flying with a rocket!!!");
    }
}
