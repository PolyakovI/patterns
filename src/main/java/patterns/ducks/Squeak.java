package patterns.ducks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by ipolyakov on 08.11.2017.
 */
public class Squeak implements QuackBehavior {
    private static final Logger logger = LogManager.getLogger(Squeak.class);
    @Override
    public void quack() {
        logger.info("Squeak");
    }
}
