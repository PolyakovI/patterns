package patterns.ducks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by ipolyakov on 08.11.2017.
 */
public class MallardDuck extends Duck {
    private static final Logger logger = LogManager.getLogger(MallardDuck.class);

    @Override
    public void display() {
        logger.info("I`m really Mallard duck!!!");
    }

    public MallardDuck() {
        quackBehavior = new Quack();
        flyBehavior = new FlyWithWings();
    }
}
