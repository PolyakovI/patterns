package patterns.ducks;

/**
 * Created by ipolyakov on 08.11.2017.
 */
public interface FlyBehavior {
    public void fly()   ;
}
