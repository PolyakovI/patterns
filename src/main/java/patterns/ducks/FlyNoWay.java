package patterns.ducks;

import org.apache.logging.log4j.Logger;

import static org.apache.logging.log4j.LogManager.getLogger;

/**
 * Created by ipolyakov on 08.11.2017.
 */
public class FlyNoWay implements FlyBehavior {
    private static final Logger logger = getLogger(FlyNoWay.class);

    @Override
    public void fly() {
        logger.info("I can`t fly");
    }
}
