package patterns.pizzeria;

public class NyPizzeriaStore extends PizzeriaStore {
    @Override
    protected Pizza createPizza(String type) {
        if (type.equals("cheese")) {
            return new NYStyleCheesePizza();
        } else return null;
    }
}
