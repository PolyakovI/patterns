package patterns.pizzeria;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PizzeriaSimulator {
    private static final Logger logger = LogManager.getLogger(PizzeriaSimulator.class);

    public static void main(String[] args) {
        PizzeriaStore nyStore = new NyPizzeriaStore();
        PizzeriaStore chicagoStore = new ChicagoPizzeriaStore();

        nyStore.orderPizza("cheese");
        logger.info("");
        chicagoStore.orderPizza("cheese");


    }
}
