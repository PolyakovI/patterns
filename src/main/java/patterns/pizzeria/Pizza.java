package patterns.pizzeria;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public abstract class Pizza {
    private static final Logger logger = LogManager.getLogger(Pizza.class);
    private String name;
    private String dough;
    private String sauce;
    private List<Topping> toppings = new ArrayList<>();

    public void prepare(){
        logger.info("Preparing" + name);
        logger.info("Tossing dough...");
        logger.info("Adding sauce...");
        logger.info("Adding Topping:");
        for (Topping topping : toppings) {
            logger.info(topping.getName());
        }
    }

    public void bake(){
        logger.info("Bagging 25 minutes at 350.");
    }

    public void cut(){
        logger.info("Cutting the pizza.");
    }

    public void box(){
        logger.info("Boxing the pizza.");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDough() {
        return dough;
    }

    public void setDough(String dough) {
        this.dough = dough;
    }

    public String getSauce() {
        return sauce;
    }

    public void setSauce(String sauce) {
        this.sauce = sauce;
    }

    public List<Topping> getToppings() {
        return toppings;
    }

    public void addTopping(Topping topping) {
        this.toppings.add(topping);
    }
}
