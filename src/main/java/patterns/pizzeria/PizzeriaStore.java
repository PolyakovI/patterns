package patterns.pizzeria;

public abstract class PizzeriaStore {

    Pizza orderPizza (String type){
        Pizza pizza = createPizza(type);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }

    protected abstract Pizza createPizza(String type);
}
