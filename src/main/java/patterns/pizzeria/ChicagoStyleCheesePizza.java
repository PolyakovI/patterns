package patterns.pizzeria;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ChicagoStyleCheesePizza extends Pizza {
    private static final Logger logger = LogManager.getLogger(ChicagoStyleCheesePizza.class);
    public ChicagoStyleCheesePizza() {
        setName("Chicago Style Deep Dish Cheese Pizza");
        setDough("Extra Thick Crust Dough");
        setSauce("Plum Tomato Sauce");
        addTopping(new Topping("Shredded Mozzarella Cheese"));
    }
    @Override
    public void cut() {
        logger.info("Cutting the pizza into square slices");
    }
}
