package patterns.pizzeria;

public class ChicagoPizzeriaStore extends PizzeriaStore {
    @Override
    protected Pizza createPizza(String type) {
        if (type.equals("cheese")) {
            return new ChicagoStyleCheesePizza();
        } else return null;
    }
}
