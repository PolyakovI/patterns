package patterns.pizzeria;

public class NYStyleCheesePizza extends Pizza {
    public NYStyleCheesePizza() {
        setName("NY Style Sauce and Cheese Pizza");
        setDough("Thin Crust Dough");
        setSauce("Marinara Sauce");
        addTopping(new Topping("Grated Reggiano Cheese"));
    }
}
