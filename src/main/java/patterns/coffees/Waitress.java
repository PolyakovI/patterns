package patterns.coffees;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Waitress {
    DinnerMenu dinnerMenu;
    PancakeHouseMenu pancakeHouseMenu;
    private static final Logger logger = LogManager.getLogger(Waitress.class);

    public Waitress(DinnerMenu dinnerMenu, PancakeHouseMenu pancakeHouseMenu) {
        this.dinnerMenu = dinnerMenu;
        this.pancakeHouseMenu = pancakeHouseMenu;
    }

    void printBreakfastMenu(){

    }

    void printLunchMenu(){}

    void printMenu(){
//        Iterator pancakeIterator = pancakeHouseMenu.
        Iterator dinerIterator = dinnerMenu.createIterator();
        logger.info("=== Lunch menu ===");
        printMenu(dinerIterator);

    }
    void printMenu(Iterator iterator){
        while (iterator.hasNext()){
            MenuItem menuItem = iterator.next();
            logger.info(menuItem.toString());
        }
    }
}
