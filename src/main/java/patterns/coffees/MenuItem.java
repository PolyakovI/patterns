package patterns.coffees;

public class MenuItem {
    private String name;
    private String description;
    private boolean vegetarian;
    private double price;

    MenuItem(String name, String description, boolean vegetarian, double price) {
        this.name = name;
        this.description = description;
        this.vegetarian = vegetarian;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isVegetarian() {
        return vegetarian;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return String.format("%s: %.2f$ --%s",getName(),getPrice(),getDescription());
    }
}
