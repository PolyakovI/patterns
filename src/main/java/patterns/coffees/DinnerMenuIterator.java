package patterns.coffees;

public class DinnerMenuIterator implements Iterator {
    private MenuItem[] items;
    int position;
    public DinnerMenuIterator(MenuItem[] items) {
        this.items = items;
        position = 0;
    }

    @Override
    public boolean hasNext() {
        return (position < items.length)&&(items[position]!=null);
    }

    @Override
    public MenuItem next() {
        return items[position++];
    }
}
