package patterns.coffees;

public interface Iterator {
    boolean hasNext();
    MenuItem next();
}
