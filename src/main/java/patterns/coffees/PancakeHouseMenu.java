package patterns.coffees;

import java.util.ArrayList;

public class PancakeHouseMenu {
    private ArrayList<MenuItem> menuItems;

    public PancakeHouseMenu() {
        menuItems = new ArrayList<>();
        menuItems.add(new MenuItem("Regular Pancake Breakfast","Two pancakes and coffee", true, 2.99));
        menuItems.add(new MenuItem("Small Pancake Breakfast","One pancake and coffee", true, 2.99));
    }

    public ArrayList<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void addMenuItems(MenuItem menuItem) {
        this.menuItems.add(menuItem);
    }
}
