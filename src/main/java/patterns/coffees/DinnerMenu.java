package patterns.coffees;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DinnerMenu {
    private static final Logger logger = LogManager.getLogger(DinnerMenu.class);
    private static final int MAX_ITEMS = 6;
    private static final String TOO_MANY_ITEMS_IN_MENU = "Too many items in menu";
    private int numberOfItem = 0;
    private MenuItem[] menuItems;

    public DinnerMenu() {
        menuItems = new MenuItem[MAX_ITEMS];
        addMenuItem(new MenuItem("BLT sandwich","Bacon, lettuce and tomato on grey Bread", false, 2.99));
        addMenuItem(new MenuItem("vegetarian BLT sandwich","Soe bacon, lettuce and tomato on grey Bread", true, 2.99));
    }

    public void addMenuItem(MenuItem menuItem) {
        if (numberOfItem < MAX_ITEMS){
            menuItems[numberOfItem] = menuItem;
            numberOfItem++;
        }
        else{
            logger.error(TOO_MANY_ITEMS_IN_MENU);
            throw new IllegalArgumentException(TOO_MANY_ITEMS_IN_MENU);
        }
    }

    public void setMenuItem(int index,MenuItem menuItem) {
        menuItems[index] = menuItem;
    }

    Iterator createIterator(){
        return new DinnerMenuIterator(menuItems);
    }
}
