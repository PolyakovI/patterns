package patterns.starbuzz;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import patterns.starbuzz.beverages.Cappuccino;
import patterns.starbuzz.beverages.Espresso;
import patterns.starbuzz.condiments.Mocha;
import patterns.starbuzz.condiments.Soy;
import patterns.starbuzz.condiments.Whip;

class BeverageTest {
    @Test
    void cost() {
        Beverage beverage = new Cappuccino(Beverage.size.SMALL);
        beverage = new Mocha(beverage);
        beverage = new Mocha(beverage);
        beverage = new Whip(beverage);
        beverage = new Soy(beverage);
        Assertions.assertEquals(1.75,beverage.cost(),1e-5);
    }

    @Test
    void cost1() {
        Beverage beverage = new Espresso();
        beverage = new Mocha(beverage);
        beverage = new Mocha(beverage);
        beverage = new Whip(beverage);
        beverage = new Soy(beverage);
        Assertions.assertEquals(2.59,beverage.cost(),1e-5);
    }

    @Test
    void getDescription() {
        Beverage beverage = new Cappuccino();
        beverage = new Mocha(beverage);
        beverage = new Whip(beverage);
        beverage = new Soy(beverage);
        Assertions.assertEquals(beverage.getDescription(),"It is Cappuccino, Mocha, Whip, Soy");
    }

}