package patterns.smarthouse;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import patterns.smarthouse.commands.GarageDoorOpenCommand;
import patterns.smarthouse.commands.LightOnCommand;
import patterns.smarthouse.devices.GarageDoor;
import patterns.smarthouse.devices.Light;

class SimpleRemoteControlTest {

    private static SimpleRemoteControl remoteControl;

    @BeforeAll
    static void init(){
        remoteControl = new SimpleRemoteControl();
        remoteControl.setSlot1(new LightOnCommand(new Light()));
        remoteControl.setSlot2(new GarageDoorOpenCommand(new GarageDoor()));
    }

    @Test
    void buttonSlot1WasPressedTest() {
        remoteControl.buttonSlot1WasPressed();
    }
    @Test
    void buttonSlot2WasPressedTest(){
        remoteControl.buttonSlot2WasPressed();
    }
}