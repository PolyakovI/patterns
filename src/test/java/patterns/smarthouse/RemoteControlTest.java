package patterns.smarthouse;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import patterns.smarthouse.devices.Light;
import patterns.smarthouse.devices.LightInKitchen;
import patterns.smarthouse.devices.Stereo;
import patterns.smarthouse.devices.StereoInBedroom;

class RemoteControlTest {
    private static RemoteControl remoteControl;
    private static final int NUMBER_DEVICES = 7;

    @BeforeAll
    static void init(){
        remoteControl = new RemoteControl(NUMBER_DEVICES);
        remoteControl.setDevice(0,new LightInKitchen(new Light("Light in kitchen")));
        remoteControl.setDevice(1,new StereoInBedroom(new Stereo("Stereo in bedroom")));
    }

    @Test
    void onButtonWasPushed() {
        remoteControl.onButtonWasPushed(0);
        remoteControl.undoButtonWasPushed();
        remoteControl.onButtonWasPushed(1);
        remoteControl.onButtonWasPushed(2);
    }

    @Test
    void offButtonWasPushed() {
        remoteControl.offButtonWasPushed(0);
        remoteControl.offButtonWasPushed(1);
        remoteControl.undoButtonWasPushed();
        remoteControl.offButtonWasPushed(2);
    }

}